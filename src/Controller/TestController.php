<?php

namespace Drupal\commerce_plexo\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Plexo\Sdk\Client;
use Plexo\Sdk\Exception\ConfigurationException;
use Plexo\Sdk\Exception\PlexoException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides test for Plexo.
 *
 * @package Drupal\commerce_plexo\Controller
 */
class TestController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Return HtmlResponse.
   */
  public function index(): array {
    $gatewayStorage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
    $gateways = $gatewayStorage->loadByProperties([
      'plugin' => 'plexo',
    ]);
    $plugin = reset($gateways);
    $plugin = $plugin->getPlugin();
    try {
      $client = new Client([
        'client' => $plugin->getPlexoClientName(),
        'pfx_filename' => $plugin->getCertificatePath(),
        'pfx_passphrase' => $plugin->getPfxPassphrase(),
        'env' => $plugin->getMode(),
      ]);
    }
    catch (ConfigurationException $e) {
      $result = vsprintf(
        "new Client: %s, Code: %s, Message: %s",
        [
          get_class($e),
          $e->getCode(),
          $e->getMessage(),
        ]
      );
      return [
        '#type' => 'markup',
        '#markup' => $result,
      ];
    }

    try {
      $response = $client->GetCommerces();
      $result = print_r($response, TRUE);
    }
    catch (PlexoException $e) {
      $result = vsprintf(
        "GetCommerces: %s, Code: %s, Message: %s",
        [
          get_class($e),
          $e->getCode(),
          $e->getMessage(),
        ]
      );
    }
    return [
      '#type' => 'markup',
      '#markup' => '<pre>' . $result . '</pre>',
    ];
  }

}
