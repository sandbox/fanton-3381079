<?php

namespace Drupal\commerce_plexo\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Plexo\Sdk\Type\InclusionType;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "plexo",
 *   label = @Translation("Plexo (Off-site redirect)"),
 *   display_label = @Translation("Plexo"),
 *   modes= {
 *     "test" = "Test",
 *     "prod" = "Prod"
 *   }
 * )
 */
class OffsiteRedirect extends OffsitePaymentGatewayBase {

  const PLEXO_LIVE_URL = 'https://pagos.plexo.com.uy:4043/SecurePaymentGateway.svc';

  const PLEXO_SANDBOX_URL = 'https://testing.plexo.com.uy:4043/SecurePaymentGateway.svc';

  /**
   * {@inheritdoc}
   */
  public function getPfxPassphrase() {
    return $this->configuration['pfx_passphrase'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCertificate() {
    return $this->configuration['certificate'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCertificatePath() {
    $file = File::load($this->configuration['certificate'][0]);
    return \Drupal::service('file_system')->realpath($file->getFileUri());
  }

  /**
   * {@inheritdoc}
   */
  public function getPlexoClientName() {
    return $this->configuration['plexo_client_name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDebug() {
    return $this->configuration['debug'];
  }

  /**
   * {@inheritdoc}
   */
  public function getInvoicePrefix() {
    return $this->configuration['invoice_prefix'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleButton() {
    return $this->configuration['title_button'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->configuration['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function getApplicableLaw() {
    return $this->configuration['applicable_law'];
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    if ($this->getMode() === 'prod') {
      return self::PLEXO_LIVE_URL;
    }
    else {
      return self::PLEXO_SANDBOX_URL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'pfx_passphrase' => '',
      'certificate' => '',
      'plexo_client_name' => '',
      'debug' => 1,
      'invoice_prefix' => 'GC-',
      'title_button' => 'Plexo Checkout',
      'description' => 'Pay via Plexo with Master, Visa and Oca card.',
      'applicable_law' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['pfx_passphrase'] = [
      '#type' => 'password',
      '#title' => $this->t('PFX Passphrase'),
      '#description' => $this->t('Provided by Plexo.'),
      '#default_value' => $this->getPfxPassphrase(),
    ];

    // @todo validate key/certificate pairing and expiration.
    $form['certificate'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Certificate'),
      '#description' => $this->t('Upload here your PFX file and we will extract the needed information.'),
      '#upload_location' => 'private://commerce_plexo-certificates',
      '#upload_validators' => [
        'file_validate_extensions' => ['pfx'],
      ],
      '#default_value' => $this->getCertificate(),
      '#required' => TRUE,
    ];

    $form['plexo_client_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Plexo Client Name'),
      '#description' => $this->t('Provided by Plexo.'),
      '#default_value' => $this->getPlexoClientName(),
      '#required' => TRUE,
    ];

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Logging'),
      '#description' => $this->t('Log Plexo events, such as IPN requests.'),
      '#default_value' => $this->getDebug(),
    ];

    $form['invoice_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Invoice Prefix'),
      '#description' => $this->t('Please enter a prefix for your invoice numbers. If you use your Plexo account for multiple stores ensure this prefix is unique as Plexo will not allow orders with the same invoice number.'),
      '#default_value' => $this->getInvoicePrefix(),
    ];

    $form['title_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title button'),
      '#description' => $this->t('This controls the title in the button which the user sees during checkout.'),
      '#default_value' => $this->getTitleButton(),
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#description' => $this->t('This controls the description which the user sees during checkout.'),
      '#default_value' => $this->getDescription(),
    ];

    $form['applicable_law'] = [
      '#type' => 'select',
      '#title' => $this->t('Financial inclusion Law'),
      '#description' => $this->t('Financial inclusion applicable Law.'),
      '#options' => [
        InclusionType::NONE => $this->t('Not applicable'),
        InclusionType::LAW_17934 => $this->t('Law 17934'),
        InclusionType::LAW_19210 => $this->t('Law 19210'),
      ],
      '#default_value' => $this->getApplicableLaw(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    // Save certificate file.
    $certificate_file = $form_state->getValue('certificate', 0);
    if (!empty($certificate_file[0])) {
      $file = File::load($certificate_file[0]);
      $file->setPermanent();
      try {
        $file->save();
      }
      catch (EntityStorageException $e) {
        $this->logger('commerce_plexo')->error($e->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $old_configuration = $this->getConfiguration();
    $old_pass = $old_configuration['pfx_passphrase'] ?? '';

    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['pfx_passphrase'] = empty($values['pfx_passphrase']) ? $old_pass : $values['pfx_passphrase'];
      $this->configuration['certificate'] = $values['certificate'];
      $this->configuration['plexo_client_name'] = $values['plexo_client_name'];
      $this->configuration['debug'] = $values['debug'];
      $this->configuration['invoice_prefix'] = $values['invoice_prefix'];
      $this->configuration['title_button'] = $values['title_button'];
      $this->configuration['description'] = $values['description'];
      $this->configuration['applicable_law'] = $values['applicable_law'];
    }
  }

}
